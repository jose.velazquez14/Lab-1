package orderedStructures;

public class Geometric extends Progression {

	private double commonFactor; 
	
	public Geometric(double firstValue, double commonFactor) { 
		super(firstValue); 
		this.commonFactor = commonFactor; 
	}
	
	@Override
	public double nextValue() throws IllegalStateException {
		if(this.didFirstRun==false)
			throw new IllegalStateException(); 
		current = current * commonFactor; 
		return current;
	}


	public String toString(){
		return "Geom(" + (int) this.firstValue() + "," + (int) commonFactor + ")";
	}
	
	
	public double getTerm(int n){
		
		return this.firstValue()*(Math.pow(commonFactor, n-1));
	}
}
